package exercise.coding.a2019_14_03_lawrenceclarke_nycschools.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;

import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.R;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.fragment.NYCSchoolDetailsFragment;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.fragment.NYCSchoolDirectoryFragment;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model.NYCSchool;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model.NYCSchoolDetail;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model.NYCSchoolSAT;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.network.Api;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.network.Requests;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NYCSchoolDirectoryActivity extends AppCompatActivity {

    private static final String TAG = NYCSchoolDirectoryActivity.class.getSimpleName();

    private Requests requests;
    private ProgressBar progressBar;
    private ArrayList<NYCSchool> nycSchools;
    private  ArrayList<NYCSchoolSAT> nycSchoolSATs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nycschool_directory);

        requests = Api.getRequests();
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        getNYCSchoolDirectory();
        getNYCSchoolDirectorySATs();
    }

    //Loads a fragment
    public boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    //Gets the NYC School Directory
    private void getNYCSchoolDirectory() {
        requests.getNYCSchoolDirectory().enqueue(new Callback<ArrayList<NYCSchool>>() {
            @Override
            public void onResponse(Call<ArrayList<NYCSchool>> call, Response<ArrayList<NYCSchool>> response) {
                if(response.isSuccessful()) {
                    nycSchools = response.body();
                    loadFragment(NYCSchoolDirectoryFragment.newInstance(nycSchools));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<NYCSchool>> call, Throwable t) {
                showErrorMessage(t.getMessage());
            }
        });
    }

    //Gets all the SATs for NYCSchools
    private void getNYCSchoolDirectorySATs() {
        requests.getNYCSchoolSATs().enqueue(new Callback<ArrayList<NYCSchoolSAT>>() {
            @Override
            public void onResponse(Call<ArrayList<NYCSchoolSAT>> call, Response<ArrayList<NYCSchoolSAT>> response) {
                nycSchoolSATs = response.body();
                setProgressBarVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ArrayList<NYCSchoolSAT>> call, Throwable t) {
                showErrorMessage(t.getMessage());
            }
        });
    }

    //Displays a progressbar
    private void setProgressBarVisibility(int visibility) {
        progressBar.setVisibility(visibility);
    }

    //Gets the NYCSchoolSat
    public void getNYCSchoolSAT(String dbn) {
        NYCSchoolDetail nycSchoolDetail = new NYCSchoolDetail();
        for (NYCSchoolSAT nycSchoolSAT: nycSchoolSATs) {
            if (dbn.equals(nycSchoolSAT.getDbn())) {
                nycSchoolDetail.setSchoolName(nycSchoolSAT.getSchoolName());
                nycSchoolDetail.setSatCriticalReadingAvgScore(nycSchoolSAT.getSatCriticalReadingAvgScore());
                nycSchoolDetail.setSatMathAvgScore(nycSchoolSAT.getSatMathAvgScore());
                nycSchoolDetail.setSatWritingAvgScore(nycSchoolSAT.getSatWritingAvgScore());
            }
        }

        for (NYCSchool nycSchool: nycSchools) {
            if (nycSchoolDetail.getSchoolName().equals(nycSchool.getSchoolName())) {
                nycSchoolDetail.setCity(nycSchool.getCity());
                nycSchoolDetail.setPhoneNumber(nycSchool.getPhoneNumber());
                nycSchoolDetail.setWebsite(nycSchool.getWebsite());
                nycSchoolDetail.setZip(nycSchool.getZip());
                nycSchoolDetail.setSchoolEmail(nycSchool.getSchoolEmail());
            }
        }
        loadFragment(NYCSchoolDetailsFragment.newInstance(nycSchoolDetail));
    }

    //Logs the error message of the endpoint
    private void showErrorMessage(String message) {
        Log.d(TAG, message);
    }
}
