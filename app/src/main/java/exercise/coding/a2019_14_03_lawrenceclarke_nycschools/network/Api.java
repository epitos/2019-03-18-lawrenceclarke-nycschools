package exercise.coding.a2019_14_03_lawrenceclarke_nycschools.network;

public class Api {
    public static final String BASE_URL =  "https://data.cityofnewyork.us/resource/";

    public static Requests getRequests() {
        return RetrofitClient.getClient(BASE_URL).create(Requests.class);
    }
}
