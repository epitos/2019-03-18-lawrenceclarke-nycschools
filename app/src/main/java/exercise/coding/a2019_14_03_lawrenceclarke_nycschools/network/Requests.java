package exercise.coding.a2019_14_03_lawrenceclarke_nycschools.network;

import java.util.ArrayList;

import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model.NYCSchool;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model.NYCSchoolSAT;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Requests {

    //Endpoint fot getting NYC School Directory
    @GET("s3k6-pzi2.json")
    Call<ArrayList<NYCSchool>> getNYCSchoolDirectory();

    //Endpoint fot getting NYC School Sats
    @GET("f9bf-2cp4.json")
    Call<ArrayList<NYCSchoolSAT>> getNYCSchoolSATs();
}
