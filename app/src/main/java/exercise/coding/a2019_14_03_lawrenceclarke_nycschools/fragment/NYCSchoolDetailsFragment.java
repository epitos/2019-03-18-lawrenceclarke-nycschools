package exercise.coding.a2019_14_03_lawrenceclarke_nycschools.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.R;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model.NYCSchoolDetail;

public class NYCSchoolDetailsFragment extends Fragment {

    private static final String ARG_NYC_DETAIL = "nyc_school_detail";

    private NYCSchoolDetail schoolDetail;
    private TextView schoolName;
    private TextView city;
    private TextView email;
    private TextView phoneNumber;
    private TextView website;
    private TextView zip;
    private TextView mathSatResult;
    private TextView writingSatResult;
    private TextView readingSatResult;

    //Creates a instance of the NYCSchoolDetailsFragment
    public static NYCSchoolDetailsFragment newInstance(NYCSchoolDetail schoolDetail) {
        NYCSchoolDetailsFragment fragment = new NYCSchoolDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_NYC_DETAIL, schoolDetail);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            schoolDetail = getArguments().getParcelable(ARG_NYC_DETAIL);
            setSchoolDetail(schoolDetail);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_nycschool_details, container, false);
        bindViews(v);
        return v;
    }

    private void bindViews(View v) {
        schoolName = (TextView) v.findViewById(R.id.school_name);
        city = (TextView) v.findViewById(R.id.city);
        email = (TextView) v.findViewById(R.id.school_email);
        phoneNumber = (TextView) v.findViewById(R.id.school_phone_number);
        website = (TextView) v.findViewById(R.id.school_website);
        zip = (TextView) v.findViewById(R.id.school_zip);
        mathSatResult = (TextView) v.findViewById(R.id.math_score);
        writingSatResult = (TextView) v.findViewById(R.id.writing_score);
        readingSatResult = (TextView) v.findViewById(R.id.reading_score);
    }

    //Sets school information
    private void setSchoolDetail(NYCSchoolDetail nycSchoolDetail) {
        schoolName.setText(nycSchoolDetail.getSchoolName());
        city.setText(nycSchoolDetail.getCity());
        email.setText(nycSchoolDetail.getSchoolEmail());
        phoneNumber.setText(nycSchoolDetail.getPhoneNumber());
        website.setText(nycSchoolDetail.getWebsite());
        zip.setText(nycSchoolDetail.getZip());
        mathSatResult.setText(nycSchoolDetail.getSatMathAvgScore());
        writingSatResult.setText(nycSchoolDetail.getSatWritingAvgScore());
        readingSatResult.setText(nycSchoolDetail.getSatCriticalReadingAvgScore());
    }
}
