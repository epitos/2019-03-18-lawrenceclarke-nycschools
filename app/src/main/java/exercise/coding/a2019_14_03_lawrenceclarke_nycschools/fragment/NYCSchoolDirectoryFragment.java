package exercise.coding.a2019_14_03_lawrenceclarke_nycschools.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.R;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.adapter.NYCSchoolDetailsAdapter;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model.NYCSchool;

public class NYCSchoolDirectoryFragment extends Fragment {
    private static final String ARG_NYC_SCHOOLS = "nyc_schools";

    private ArrayList<NYCSchool> nycSchoolNameList;
    private RecyclerView recyclerView;
    private NYCSchoolDetailsAdapter adapter;
    private Activity activity;

    //Creates a instance of the NYCSchoolDirectoryFragment
    public static NYCSchoolDirectoryFragment newInstance(ArrayList<NYCSchool> nycSchoolNameList) {
        NYCSchoolDirectoryFragment fragment = new NYCSchoolDirectoryFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_NYC_SCHOOLS, nycSchoolNameList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            nycSchoolNameList = getArguments().getParcelableArrayList(ARG_NYC_SCHOOLS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_nycschool_directory, container, false);
        bindViews(v);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setNYCSchoolList(nycSchoolNameList);
    }

    private void bindViews(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.school_directory_list);
    }

    //Sets the list of NYC Schools
    private void setNYCSchoolList(ArrayList<NYCSchool> nycSchoolNameList) {
        adapter = new NYCSchoolDetailsAdapter(activity, nycSchoolNameList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(activity,  DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setAdapter(adapter);
    }
}
