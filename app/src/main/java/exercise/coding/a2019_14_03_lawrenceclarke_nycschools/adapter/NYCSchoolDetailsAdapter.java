package exercise.coding.a2019_14_03_lawrenceclarke_nycschools.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.R;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.activity.NYCSchoolDirectoryActivity;
import exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model.NYCSchool;

public class NYCSchoolDetailsAdapter extends RecyclerView.Adapter<NYCSchoolDetailsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<NYCSchool> nycSchools;

    public NYCSchoolDetailsAdapter(Context context, ArrayList<NYCSchool> nycSchools) {
        this.context = context;
        this.nycSchools = nycSchools;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View v = inflater.inflate(R.layout.nyc_school, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int pos) {
        viewHolder.schoolName.setText(nycSchools.get(pos).getSchoolName());

        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NYCSchoolDirectoryActivity) context).getNYCSchoolSAT(nycSchools.get(pos).getDbn());
            }
        });
    }

    @Override
    public int getItemCount() {
        return nycSchools.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView schoolName;
        public LinearLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);

            schoolName = (TextView) itemView.findViewById(R.id.school_name);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);

        }
    }
}
