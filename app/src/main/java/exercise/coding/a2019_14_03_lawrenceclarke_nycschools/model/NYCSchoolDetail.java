package exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NYCSchoolDetail implements Parcelable {

    private String satCriticalReadingAvgScore;
    private String satMathAvgScore;
    private String satWritingAvgScore;
    private String schoolName;
    private String city;
    private String schoolEmail;
    private String phoneNumber;
    private String zip;
    private String website;

    public NYCSchoolDetail() {
    }

    protected NYCSchoolDetail(Parcel in) {
        satCriticalReadingAvgScore = in.readString();
        satMathAvgScore = in.readString();
        satWritingAvgScore = in.readString();
        schoolName = in.readString();
        city = in.readString();
        schoolEmail = in.readString();
        phoneNumber = in.readString();
        zip = in.readString();
        website = in.readString();
    }

    public static final Creator<NYCSchoolDetail> CREATOR = new Creator<NYCSchoolDetail>() {
        @Override
        public NYCSchoolDetail createFromParcel(Parcel in) {
            return new NYCSchoolDetail(in);
        }

        @Override
        public NYCSchoolDetail[] newArray(int size) {
            return new NYCSchoolDetail[size];
        }
    };

    public String getSatCriticalReadingAvgScore() {
        return satCriticalReadingAvgScore;
    }

    public void setSatCriticalReadingAvgScore(String satCriticalReadingAvgScore) {
        this.satCriticalReadingAvgScore = satCriticalReadingAvgScore;
    }

    public String getSatMathAvgScore() {
        return satMathAvgScore;
    }

    public void setSatMathAvgScore(String satMathAvgScore) {
        this.satMathAvgScore = satMathAvgScore;
    }

    public String getSatWritingAvgScore() {
        return satWritingAvgScore;
    }

    public void setSatWritingAvgScore(String satWritingAvgScore) {
        this.satWritingAvgScore = satWritingAvgScore;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(satCriticalReadingAvgScore);
        parcel.writeString(satMathAvgScore);
        parcel.writeString(satWritingAvgScore);
        parcel.writeString(schoolName);
        parcel.writeString(city);
        parcel.writeString(schoolEmail);
        parcel.writeString(phoneNumber);
        parcel.writeString(zip);
        parcel.writeString(website);
    }
}
