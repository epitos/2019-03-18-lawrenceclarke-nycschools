package exercise.coding.a2019_14_03_lawrenceclarke_nycschools.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class NYCSchool implements Parcelable {
    @SerializedName("academicopportunities1")
    private String academicopportunities1;
    @SerializedName("academicopportunities2")
    private String academicopportunities2;
    @SerializedName("admissionspriority11")
    private String admissionspriority11;
    @SerializedName("admissionspriority21")
    private String admissionspriority21;
    @SerializedName("admissionspriority31")
    private String admissionspriority31;
    @SerializedName("attendance_rate")
    private String attendanceRate;
    @SerializedName("bbl")
    private String bbl;
    @SerializedName("bin")
    private String bin;
    @SerializedName("boro")
    private String boro;
    @SerializedName("borough")
    private String borough;
    @SerializedName("building_code")
    private String buildingCode;
    @SerializedName("bus")
    private String bus;
    @SerializedName("census_tract")
    private String censusTract;
    @SerializedName("city")
    private String city;
    @SerializedName("code1")
    private String code1;
    @SerializedName("community_board")
    private String communityBoard;
    @SerializedName("council_district")
    private String councilDistrict;
    @SerializedName("dbn")
    private String dbn;
    @SerializedName("directions1")
    private String directions1;
    @SerializedName("ell_programs")
    private String ellPrograms;
    @SerializedName("extracurricular_activities")
    private String extracurricularActivities;
    @SerializedName("fax_number")
    private String faxNumber;
    @SerializedName("finalgrades")
    private String finalgrades;
    @SerializedName("grade9geapplicants1")
    private String grade9geapplicants1;
    @SerializedName("grade9geapplicantsperseat1")
    private String grade9geapplicantsperseat1;
    @SerializedName("grade9gefilledflag1")
    private String grade9gefilledflag1;
    @SerializedName("grade9swdapplicants1")
    private String grade9swdapplicants1;
    @SerializedName("grade9swdapplicantsperseat1")
    private String grade9swdapplicantsperseat1;
    @SerializedName("grade9swdfilledflag1")
    private String grade9swdfilledflag1;
    @SerializedName("grades2018")
    private String grades2018;
    @SerializedName("interest1")
    private String interest1;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("location")
    private String location;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("method1")
    private String method1;
    @SerializedName("neighborhood")
    private String neighborhood;
    @SerializedName("nta")
    private String nta;
    @SerializedName("offer_rate1")
    private String offerRate1;
    @SerializedName("overview_paragraph")
    private String overviewParagraph;
    @SerializedName("pct_stu_enough_variety")
    private String pctStuEnoughVariety;
    @SerializedName("pct_stu_safe")
    private String pctStuSafe;
    @SerializedName("phone_number")
    private String phoneNumber;
    @SerializedName("primary_address_line_1")
    private String primaryAddressLine1;
    @SerializedName("program1")
    private String program1;
    @SerializedName("requirement1_1")
    private String requirement11;
    @SerializedName("requirement2_1")
    private String requirement21;
    @SerializedName("requirement3_1")
    private String requirement31;
    @SerializedName("requirement4_1")
    private String requirement41;
    @SerializedName("requirement5_1")
    private String requirement51;
    @SerializedName("school_10th_seats")
    private String school10thSeats;
    @SerializedName("school_accessibility_description")
    private String schoolAccessibilityDescription;
    @SerializedName("school_email")
    private String schoolEmail;
    @SerializedName("school_name")
    private String schoolName;
    @SerializedName("school_sports")
    private String schoolSports;
    @SerializedName("seats101")
    private String seats101;
    @SerializedName("seats9ge1")
    private String seats9ge1;
    @SerializedName("seats9swd1")
    private String seats9swd1;
    @SerializedName("state_code")
    private String stateCode;
    @SerializedName("subway")
    private String subway;
    @SerializedName("total_students")
    private String totalStudents;
    @SerializedName("website")
    private String website;
    @SerializedName("zip")
    private String zip;
    @SerializedName("academicopportunities3")
    private String academicopportunities3;
    @SerializedName("addtl_info1")
    private String addtlInfo1;
    @SerializedName("eligibility1")
    private String eligibility1;
    @SerializedName("language_classes")
    private String languageClasses;
    @SerializedName("transfer")
    private String transfer;

    protected NYCSchool(Parcel in) {
        academicopportunities1 = in.readString();
        academicopportunities2 = in.readString();
        admissionspriority11 = in.readString();
        admissionspriority21 = in.readString();
        admissionspriority31 = in.readString();
        attendanceRate = in.readString();
        bbl = in.readString();
        bin = in.readString();
        boro = in.readString();
        borough = in.readString();
        buildingCode = in.readString();
        bus = in.readString();
        censusTract = in.readString();
        city = in.readString();
        code1 = in.readString();
        communityBoard = in.readString();
        councilDistrict = in.readString();
        dbn = in.readString();
        directions1 = in.readString();
        ellPrograms = in.readString();
        extracurricularActivities = in.readString();
        faxNumber = in.readString();
        finalgrades = in.readString();
        grade9geapplicants1 = in.readString();
        grade9geapplicantsperseat1 = in.readString();
        grade9gefilledflag1 = in.readString();
        grade9swdapplicants1 = in.readString();
        grade9swdapplicantsperseat1 = in.readString();
        grade9swdfilledflag1 = in.readString();
        grades2018 = in.readString();
        interest1 = in.readString();
        latitude = in.readString();
        location = in.readString();
        longitude = in.readString();
        method1 = in.readString();
        neighborhood = in.readString();
        nta = in.readString();
        offerRate1 = in.readString();
        overviewParagraph = in.readString();
        pctStuEnoughVariety = in.readString();
        pctStuSafe = in.readString();
        phoneNumber = in.readString();
        primaryAddressLine1 = in.readString();
        program1 = in.readString();
        requirement11 = in.readString();
        requirement21 = in.readString();
        requirement31 = in.readString();
        requirement41 = in.readString();
        requirement51 = in.readString();
        school10thSeats = in.readString();
        schoolAccessibilityDescription = in.readString();
        schoolEmail = in.readString();
        schoolName = in.readString();
        schoolSports = in.readString();
        seats101 = in.readString();
        seats9ge1 = in.readString();
        seats9swd1 = in.readString();
        stateCode = in.readString();
        subway = in.readString();
        totalStudents = in.readString();
        website = in.readString();
        zip = in.readString();
        academicopportunities3 = in.readString();
        addtlInfo1 = in.readString();
        eligibility1 = in.readString();
        languageClasses = in.readString();
        transfer = in.readString();
    }

    public static final Creator<NYCSchool> CREATOR = new Creator<NYCSchool>() {
        @Override
        public NYCSchool createFromParcel(Parcel in) {
            return new NYCSchool(in);
        }

        @Override
        public NYCSchool[] newArray(int size) {
            return new NYCSchool[size];
        }
    };

    public String getAcademicopportunities1() {
        return academicopportunities1;
    }

    public String getAcademicopportunities2() {
        return academicopportunities2;
    }

    public String getAdmissionspriority11() {
        return admissionspriority11;
    }

    public String getAdmissionspriority21() {
        return admissionspriority21;
    }

    public String getAdmissionspriority31() {
        return admissionspriority31;
    }

    public String getAttendanceRate() {
        return attendanceRate;
    }

    public String getBbl() {
        return bbl;
    }

    public String getBin() {
        return bin;
    }

    public String getBoro() {
        return boro;
    }

    public String getBorough() {
        return borough;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public String getBus() {
        return bus;
    }

    public String getCensusTract() {
        return censusTract;
    }

    public String getCity() {
        return city;
    }

    public String getCode1() {
        return code1;
    }

    public String getCommunityBoard() {
        return communityBoard;
    }

    public String getCouncilDistrict() {
        return councilDistrict;
    }

    public String getDbn() {
        return dbn;
    }

    public String getDirections1() {
        return directions1;
    }

    public String getEllPrograms() {
        return ellPrograms;
    }

    public String getExtracurricularActivities() {
        return extracurricularActivities;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public String getFinalgrades() {
        return finalgrades;
    }

    public String getGrade9geapplicants1() {
        return grade9geapplicants1;
    }

    public String getGrade9geapplicantsperseat1() {
        return grade9geapplicantsperseat1;
    }

    public String getGrade9gefilledflag1() {
        return grade9gefilledflag1;
    }

    public String getGrade9swdapplicants1() {
        return grade9swdapplicants1;
    }

    public String getGrade9swdapplicantsperseat1() {
        return grade9swdapplicantsperseat1;
    }

    public String getGrade9swdfilledflag1() {
        return grade9swdfilledflag1;
    }

    public String getGrades2018() {
        return grades2018;
    }

    public String getInterest1() {
        return interest1;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLocation() {
        return location;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getMethod1() {
        return method1;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public String getNta() {
        return nta;
    }

    public String getOfferRate1() {
        return offerRate1;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public String getPctStuEnoughVariety() {
        return pctStuEnoughVariety;
    }

    public String getPctStuSafe() {
        return pctStuSafe;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPrimaryAddressLine1() {
        return primaryAddressLine1;
    }

    public String getProgram1() {
        return program1;
    }

    public String getRequirement11() {
        return requirement11;
    }

    public String getRequirement21() {
        return requirement21;
    }

    public String getRequirement31() {
        return requirement31;
    }

    public String getRequirement41() {
        return requirement41;
    }

    public String getRequirement51() {
        return requirement51;
    }

    public String getSchool10thSeats() {
        return school10thSeats;
    }

    public String getSchoolAccessibilityDescription() {
        return schoolAccessibilityDescription;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getSchoolSports() {
        return schoolSports;
    }

    public String getSeats101() {
        return seats101;
    }

    public String getSeats9ge1() {
        return seats9ge1;
    }

    public String getSeats9swd1() {
        return seats9swd1;
    }

    public String getStateCode() {
        return stateCode;
    }

    public String getSubway() {
        return subway;
    }

    public String getTotalStudents() {
        return totalStudents;
    }

    public String getWebsite() {
        return website;
    }

    public String getZip() {
        return zip;
    }

    public String getAcademicopportunities3() {
        return academicopportunities3;
    }

    public String getAddtlInfo1() {
        return addtlInfo1;
    }

    public String getEligibility1() {
        return eligibility1;
    }

    public String getLanguageClasses() {
        return languageClasses;
    }

    public String getTransfer() {
        return transfer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(academicopportunities1);
        parcel.writeString(academicopportunities2);
        parcel.writeString(admissionspriority11);
        parcel.writeString(admissionspriority21);
        parcel.writeString(admissionspriority31);
        parcel.writeString(attendanceRate);
        parcel.writeString(bbl);
        parcel.writeString(bin);
        parcel.writeString(boro);
        parcel.writeString(borough);
        parcel.writeString(buildingCode);
        parcel.writeString(bus);
        parcel.writeString(censusTract);
        parcel.writeString(city);
        parcel.writeString(code1);
        parcel.writeString(communityBoard);
        parcel.writeString(councilDistrict);
        parcel.writeString(dbn);
        parcel.writeString(directions1);
        parcel.writeString(ellPrograms);
        parcel.writeString(extracurricularActivities);
        parcel.writeString(faxNumber);
        parcel.writeString(finalgrades);
        parcel.writeString(grade9geapplicants1);
        parcel.writeString(grade9geapplicantsperseat1);
        parcel.writeString(grade9gefilledflag1);
        parcel.writeString(grade9swdapplicants1);
        parcel.writeString(grade9swdapplicantsperseat1);
        parcel.writeString(grade9swdfilledflag1);
        parcel.writeString(grades2018);
        parcel.writeString(interest1);
        parcel.writeString(latitude);
        parcel.writeString(location);
        parcel.writeString(longitude);
        parcel.writeString(method1);
        parcel.writeString(neighborhood);
        parcel.writeString(nta);
        parcel.writeString(offerRate1);
        parcel.writeString(overviewParagraph);
        parcel.writeString(pctStuEnoughVariety);
        parcel.writeString(pctStuSafe);
        parcel.writeString(phoneNumber);
        parcel.writeString(primaryAddressLine1);
        parcel.writeString(program1);
        parcel.writeString(requirement11);
        parcel.writeString(requirement21);
        parcel.writeString(requirement31);
        parcel.writeString(requirement41);
        parcel.writeString(requirement51);
        parcel.writeString(school10thSeats);
        parcel.writeString(schoolAccessibilityDescription);
        parcel.writeString(schoolEmail);
        parcel.writeString(schoolName);
        parcel.writeString(schoolSports);
        parcel.writeString(seats101);
        parcel.writeString(seats9ge1);
        parcel.writeString(seats9swd1);
        parcel.writeString(stateCode);
        parcel.writeString(subway);
        parcel.writeString(totalStudents);
        parcel.writeString(website);
        parcel.writeString(zip);
        parcel.writeString(academicopportunities3);
        parcel.writeString(addtlInfo1);
        parcel.writeString(eligibility1);
        parcel.writeString(languageClasses);
        parcel.writeString(transfer);
    }
}
